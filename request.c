#include "request.h"

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#define BUFFER_SIZE 4096

char *Empty_String="";

const char* request_method_strings[]={
	"GET",
	"POST",
	NULL
};

void request_line_read(int fd,char* buffer,size_t size) {
	size_t i;
	for (i = 0,--size;i < size
		&& read(fd,&buffer[i],1) > 0 && buffer[i++] != '\n';);
	buffer[i] = 0;
}

void request_parameter_node_add(struct request_parameter_node **node,
				char *key,char *value) {
	struct request_parameter_node *newn =
	(struct request_parameter_node*)
	malloc(sizeof(struct request_parameter_node));
	newn->key = key;
	newn->value = value;
	newn->next = NULL;
	if (!*node) {
		*node = newn;
	} else {
		struct request_parameter_node *tmpn;
		for(tmpn = *node;tmpn->next;tmpn = tmpn->next);
		tmpn->next = newn;
	}
}

void request_parameter_nodes_destroy(struct request_parameter_node **node) {
	struct request_parameter_node *tmpn;
	for(;(tmpn = *node);) {
		*node = tmpn->next;
		free(tmpn);
	}
}

void request_parameter_nodes_print(const struct request_parameter_node *node) {
	int i;
	for(i = 1;node;node = node->next,++i) {
		printf("%d : '%s' -> '%s'\n",i,node->key,node->value);
	}
}

void request_parameters_parse(struct request_object *ro) {
	char *str,*saveptr;
	for (str = strtok_r(ro->parameter_blob,"&",&saveptr);str;
		str = strtok_r(NULL,"&",&saveptr)) {
		char *key,*saveptr2;
		if ((key = strtok_r(str,"=",&saveptr2))) {
			char *value;
			if (!(value = strtok_r(NULL,"=",&saveptr2))) {
				value = Empty_String;
			}
			request_parameter_node_add(&(ro->parameters),
				key,value);
		}
	}
}

void request_path_parse(char *path,struct request_object *ro) {
	char *str,*saveptr;
	if(!(str = strtok_r(path,"?",&saveptr))) {
		return;
	}
	ro->path = strdup(str);
	if (ro->method == METHOD_GET && (str = strtok_r(NULL,"?",&saveptr))) {
		ro->parameter_blob = strdup(str);
		request_parameters_parse(ro);
	}
}

void request_line_parse(char* line,struct request_object *ro) {
	int i;
	char *str,*saveptr;
	for (i=0,str = strtok_r(line," \r\n",&saveptr);str;
	    str = strtok_r(NULL," \r\n",&saveptr),++i) {
		if (i == 0) {
			// Method
			enum request_method rm;
			for (rm = 0;rm != METHOD_OTHER
				&& strcmp(str,request_method_strings[rm]);++rm);
			ro->method = rm;
		} else if (i == 1) {
			request_path_parse(str,ro);
		}
	}
}

void request_content_length_parse(char* line,struct request_object *ro) {
	char *str,*saveptr;
	if ((str = strtok_r(line,": \r\n",&saveptr))
		&& !strcmp(str,"Content-Length")
		&& (str = strtok_r(NULL,": \r\n",&saveptr))) {
		errno = 0;
		ro->contentlength = strtol(str,NULL,10);
		if (errno) {
			ro->contentlength = 0;
		}
	}
}

void request_header_parse(struct request_object *ro) {
	char buffer[BUFFER_SIZE];
	request_line_read(ro->ci.conn,buffer,BUFFER_SIZE);
	if(!buffer[0] || buffer[0] == '\r' || buffer[0] == '\n') {
		return;
	}
	request_line_parse(buffer,ro);
	request_line_read(ro->ci.conn,buffer,BUFFER_SIZE);
	while (buffer[0] && buffer[0] != '\r' && buffer[0] != '\n') {
		request_content_length_parse(buffer,ro);
		request_line_read(ro->ci.conn,buffer,BUFFER_SIZE);
	}
}

void request_body_parse(struct request_object *ro) {
	char *buffer = (char*)malloc(ro->contentlength + 1);
	ssize_t rsize,size;
	for (size = 0;
	     size < ro->contentlength
	      && (rsize = read(ro->ci.conn,buffer+size,
			((ro->contentlength - size) > BUFFER_SIZE)?
			BUFFER_SIZE:(ro->contentlength - size))) > 0;
	    size += rsize);
	buffer[size] = 0;
	ro->parameter_blob = buffer;
	request_parameters_parse(ro);
}

char *request_get_parameter(const char *key,const struct request_object *ro) {
	struct request_parameter_node *tmpn;
	for(tmpn = ro->parameters;tmpn;tmpn = tmpn->next) {
		if(!strcmp(key,tmpn->key)) {
			return tmpn->value;
		}
	}
	return NULL;
}

void request_object_init(struct request_object *ro,struct conn_info *ci) {
	memset(ro,0,sizeof(struct request_object));
	memcpy(&(ro->ci),ci,sizeof(struct conn_info));
}

void request_object_destroy(struct request_object *ro) {
	if (ro->path) free(ro->path);
	if (ro->parameters) request_parameter_nodes_destroy(&(ro->parameters));
	if (ro->parameter_blob) free(ro->parameter_blob);
}
