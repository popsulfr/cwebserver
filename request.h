#ifndef _REQUEST_H_
#define _REQUEST_H_

#include "server.h"

enum request_method {
	METHOD_GET=0,
	METHOD_POST,
	METHOD_OTHER
};

struct request_parameter_node {
	char *key; /* DO NOT FREE THIS POINTER TO parameter_blob */
	char *value; /* DO NOT FREE THIS POINTER TO parameter_blob */
	struct request_parameter_node* next;
};

struct request_object {
	struct conn_info ci;
	enum request_method method;
	char *path;
	struct request_parameter_node* parameters;
	long int contentlength;
	char* parameter_blob;
};

extern const char* request_method_strings[];

void request_object_init(struct request_object *ro,struct conn_info *ci);
void request_object_destroy(struct request_object *ro);
void request_parameter_nodes_print(const struct request_parameter_node *node);
void request_header_parse(struct request_object *ro);
void request_body_parse(struct request_object *ro);
char *request_get_parameter(const char *key,const struct request_object *ro);

#endif
