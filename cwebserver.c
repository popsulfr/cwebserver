#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include <netdb.h>

#define LISTENQUEUE 10

#define BUFFER_SIZE 4096

#define HTTP_HEADER "HTTP/1.1 200 OK\r\n\
Content-Type: text/html; charset=UTF-8\r\n\r\n"

#define HTML_CONTENT "<!DOCTYPE html>\n\
<html>\n\
	<head>\n\
		<title>Test</title>\n\
		<meta charset=\"utf-8\">\n\
	</head>\n\
	<body>\n\
		<h1>Hello world</h1>\n\
	</body>\n\
</html>\n"

struct conn_info {
	int conn;
	struct sockaddr_in inaddr;
	socklen_t inaddr_size;
};

struct conn_info_wrapper {
	struct conn_info *ci;
	pthread_t thread;
	pthread_mutex_t *mutex;
	pthread_cond_t *cond;
};

struct server_info {
	uint16_t port;
	int maxconn;
	int sock;
};

enum request_method {
	METHOD_GET=0,
	METHOD_POST,
	METHOD_OTHER
};

char* request_method_strings[]={
	"GET",
	"POST",
	NULL
};

struct request_parameter_node {
	char *key;
	char *value;
	struct request_parameter_node* next;
};

struct request_object {
	struct conn_info ci;
	enum request_method method;
	char *path;
	struct request_parameter_node* parameters;
	long int contentlength;
	char* parameter_blob;
};

char *Empty_String="";

struct server_info SI;

void close_socket(int sock) {
	if(sock<0) return;
	shutdown(sock,SHUT_RDWR);
	close(sock);
}

void die_handler(int signum) {
	(void)signum;
	close_socket(SI.sock);
}

void print_client_info(struct sockaddr_in *addr) {
	char hbuf[NI_MAXHOST];
	int err;
	if ((err=getnameinfo((struct sockaddr*)addr,
		sizeof(struct sockaddr_in),
		hbuf,NI_MAXHOST,
		NULL,0,
		NI_NUMERICHOST))!=0) {
		fprintf(stderr,
			"print_client_info(): getnameinfo(): %s\n",
			gai_strerror(err));
		return;
	}
	printf("Receiving connection from %s\n",hbuf);
}

void parse_line(int fd,char* buffer,size_t size) {
	size_t i;
	for (i = 0,--size;i < size
		&& read(fd,&buffer[i],1) > 0 && buffer[i++] != '\n';);
	buffer[i] = 0;
}

void add_request_parameter_node(struct request_parameter_node **node,
				char *key,char *value) {
	struct request_parameter_node *newn =
	(struct request_parameter_node*)
	malloc(sizeof(struct request_parameter_node));
	newn->key = key;
	newn->value = value;
	newn->next = NULL;
	if (!*node) {
		*node = newn;
	} else {
		struct request_parameter_node *tmpn;
		for(tmpn = *node;tmpn->next;tmpn = tmpn->next);
		tmpn->next = newn;
	}
}

void free_request_parameter_nodes(struct request_parameter_node **node) {
	struct request_parameter_node *tmpn;
	for(;(tmpn = *node);) {
		*node = tmpn->next;
		free(tmpn);
	}
}

void free_request_object(struct request_object *ro) {
	if (ro->path) free(ro->path);
	if (ro->parameters) free_request_parameter_nodes(&(ro->parameters));
	if (ro->parameter_blob) free(ro->parameter_blob);
}

void print_request_parameter_nodes(struct request_parameter_node *node) {
	int i;
	for(i = 0;node;node = node->next,++i) {
		printf("%d : '%s' -> '%s'\n",i,node->key,node->value);
	}
}

void parse_parameters(struct request_object *ro) {
	char *str,*saveptr;
	for (str = strtok_r(ro->parameter_blob,"&",&saveptr);str;
		str = strtok_r(NULL,"&",&saveptr)) {
		char *key,*saveptr2;
		if ((key = strtok_r(str,"=",&saveptr2))) {
			char *value;
			if (!(value = strtok_r(NULL,"=",&saveptr2))) {
				value = Empty_String;
			}
			add_request_parameter_node(&(ro->parameters),
				key,value);
		}
	}
}

void parse_request_path(char *path,struct request_object *ro) {
	char *str,*saveptr;
	if(!(str = strtok_r(path,"?",&saveptr))) {
		return;
	}
	ro->path = strdup(str);
	if (ro->method == METHOD_GET && (str = strtok_r(NULL,"?",&saveptr))) {
		ro->parameter_blob = strdup(str);
		parse_parameters(ro);
	}
}

void parse_request_line(char* line,struct request_object *ro) {
	int i;
	char *str,*saveptr;
	for (i=0,str = strtok_r(line," \r\n",&saveptr);str;
	    str=strtok_r(NULL," \r\n",&saveptr),++i) {
		if (i == 0) {
			// Method
			enum request_method rm;
			for (rm = 0;rm != METHOD_OTHER
				&& strcmp(str,request_method_strings[rm]);++rm);
			ro->method=rm;
		} else if (i == 1) {
			parse_request_path(str,ro);
		}
	}
}

void parse_request_content_length(char* line,struct request_object *ro) {
	char *str,*saveptr;
	if ((str = strtok_r(line,": \r\n",&saveptr))
		&& !strcmp(str,"Content-Length")
		&& (str = strtok_r(NULL,": \r\n",&saveptr))) {
		errno = 0;
		ro->contentlength = strtol(str,NULL,10);
		if (errno) {
			ro->contentlength = 0;
		}
	}
}

void parse_request_header(int fd,struct request_object *ro) {
	memset(ro,0,sizeof(struct request_object));
	char buffer[BUFFER_SIZE];
	parse_line(fd,buffer,BUFFER_SIZE);
	if(!buffer[0] || buffer[0] == '\r' || buffer[0] == '\n') {
		return;
	}
	parse_request_line(buffer,ro);
	parse_line(fd,buffer,BUFFER_SIZE);
	while (buffer[0] && buffer[0]!='\r' && buffer[0]!='\n') {
		parse_request_content_length(buffer,ro);
		parse_line(fd,buffer,BUFFER_SIZE);
	}
}

void parse_request_body(int fd, struct request_object *ro) {
	char *buffer = (char*)malloc(ro->contentlength + 1);
	ssize_t rsize,size;
	for (size = 0;
	     size < ro->contentlength
	      && (rsize = read(fd,buffer+size,
			((ro->contentlength - size) > BUFFER_SIZE)?
			BUFFER_SIZE:(ro->contentlength - size))) > 0;
	    size += rsize);
	buffer[size]=0;
	ro->parameter_blob = buffer;
	parse_parameters(ro);
}

char *get_request_parameter(const char *key,struct request_object *ro) {
	struct request_parameter_node *tmpn;
	for(tmpn = ro->parameters;tmpn;tmpn = tmpn->next) {
		if(!strcmp(key,tmpn->key)) {
			return tmpn->value;
		}
	}
	return NULL;
}

void *thread_process_request(void *parci) {
	struct conn_info ci;
	pthread_mutex_lock(((struct conn_info_wrapper*)parci)->mutex);
	memcpy(&ci,((struct conn_info_wrapper*)parci)->ci,
	       sizeof(struct conn_info));
	pthread_cond_signal(((struct conn_info_wrapper*)parci)->cond);
	pthread_mutex_unlock(((struct conn_info_wrapper*)parci)->mutex);
	// do stuff
	print_client_info(&(ci.inaddr));
	struct request_object ro;
	parse_request_header(ci.conn,&ro);
	if (ro.method == METHOD_POST && ro.contentlength > 0) {
		parse_request_body(ci.conn,&ro);
	}
	printf("'%s' : '%s'\n","volo",get_request_parameter("volo",&ro));
	free_request_object(&ro);
	write(ci.conn,HTTP_HEADER,strlen(HTTP_HEADER));
	write(ci.conn,HTML_CONTENT,strlen(HTML_CONTENT));
	close(ci.conn);
	return NULL;
}

int create_listener_socket(uint16_t port,int listenerqueue) {
	int sock;
	if ((sock = socket(AF_INET,SOCK_STREAM,0))<0) {
		perror("create_listener_socket(): socket()");
		return -1;
	}
	
	int yes = 1;
	if (setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(yes)) < 0) {
		perror("create_listener_socket(): setsockopt()");
		close_socket(sock);
		return -1;
	}
	
	struct sockaddr_in addr;
	memset(&addr,0,sizeof(addr));
	addr.sin_family=AF_INET;
	addr.sin_port=htons(port);
	addr.sin_addr.s_addr=INADDR_ANY;
	
	if (bind(sock,(struct sockaddr*)&addr,sizeof(addr)) < 0) {
		perror("create_listener_socket(): bind()");
		close_socket(sock);
		return -1;
	}
	
	if (listen(sock,listenerqueue) < 0) {
		perror("create_listener_socket(): listen()");
		close_socket(sock);
		return -1;
	}
	return sock;
}

int accept_connection(int sock) {
	struct conn_info ci;
	ci.inaddr_size = sizeof(ci.inaddr);
	ci.conn = accept(sock,
			(struct sockaddr*)&(ci.inaddr),&(ci.inaddr_size));
	if(ci.conn<0) {
		switch (errno) {
			case ENETDOWN:
			case EPROTO:
			case ENOPROTOOPT:
			case EHOSTDOWN:
			case ENONET:
			case EHOSTUNREACH:
			case EOPNOTSUPP:
			case ENETUNREACH:
				return 1;
			case EINTR:
				return 0;
			default:
				perror("accept_connection(): accept()");
				return 0;
		}
	}
	pthread_attr_t attr;
	if (pthread_attr_init(&attr) != 0) {
		perror("accept_connection(): pthread_attr_init()");
		close(ci.conn);
		return 1;
	}
	if (pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED) != 0) {
		perror("accept_connection(): pthread_attr_setdetachstate()");
		pthread_attr_destroy(&attr);
		close(ci.conn);
		return 1;
	}
	struct conn_info_wrapper ciw;
	ciw.ci=&ci;
	pthread_mutex_t mutex;
	pthread_cond_t cond;
	pthread_mutex_init(&mutex,NULL);
	ciw.mutex=&mutex;
	pthread_cond_init(&cond,NULL);
	ciw.cond=&cond;
	pthread_mutex_lock(ciw.mutex);
	if (pthread_create(&(ciw.thread),
		&attr,thread_process_request,(void*)&ciw) !=0 ) {
		perror("accept_connection(): pthread_create()");
	} else {
		pthread_cond_wait(ciw.cond, ciw.mutex);
	}
	pthread_mutex_unlock(ciw.mutex);
	pthread_cond_destroy(&cond);
	pthread_mutex_destroy(&mutex);
	pthread_attr_destroy(&attr);
	return 1;
}

int weblisten(struct server_info* si) {
	if ((si->sock=create_listener_socket(si->port,si->maxconn))<0) {
		return 0;
	}
	
	printf("Server is starting on port %d.\n",si->port);
	while(accept_connection(si->sock));
	printf("Server is going down.\n");
	return 1;
}

int signal_register() {
	struct sigaction dieaction;
	struct sigaction ignaction;
	memset(&dieaction,0,sizeof(dieaction));
	memset(&ignaction,0,sizeof(ignaction));
	dieaction.sa_handler = die_handler;
	ignaction.sa_handler = SIG_IGN;
	sigemptyset(&(dieaction.sa_mask));
	sigemptyset(&(ignaction.sa_mask));
	dieaction.sa_flags = 0;
	ignaction.sa_flags = 0;
	if (sigaction(SIGTERM,&dieaction,NULL) != 0
		|| sigaction(SIGINT,&dieaction,NULL) != 0
		|| sigaction(SIGHUP,&dieaction,NULL) != 0
		|| sigaction(SIGPIPE,&ignaction,NULL) != 0) {
		perror("sigaction()");
		return 0;
	}
	return 1;
}

int main() {
	SI.sock=-1;
	SI.port=8000;
	SI.maxconn=32;
	
	if (!signal_register()) {
		return EXIT_FAILURE;
	}
	
	return !weblisten(&SI);
}
