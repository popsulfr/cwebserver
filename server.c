#include "server.h"
#include "request.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

struct conn_info_wrapper {
	struct server_info *si;
	struct conn_info *ci;
	pthread_t thread;
	pthread_mutex_t *mutex;
	pthread_cond_t *cond;
};

int server_sprint_client_info(char *buffer,socklen_t size,
			       struct sockaddr_in *addr) {
	int err;
	if ((err=getnameinfo((struct sockaddr*)addr,
		sizeof(struct sockaddr_in),
		buffer,size,
		NULL,0,
		NI_NUMERICHOST)) != 0) {
		fprintf(stderr,
			"server_sprint_client_info(): getnameinfo(): %s\n",
			gai_strerror(err));
		return 0;
	}
	return 1;
}

void *server_thread_process_request(void *parci) {
	struct request_object ro;
	struct server_info *si;
	pthread_mutex_lock(((struct conn_info_wrapper*)parci)->mutex);
	si = ((struct conn_info_wrapper*)parci)->si;
	request_object_init(&ro,((struct conn_info_wrapper*)parci)->ci);
	pthread_cond_signal(((struct conn_info_wrapper*)parci)->cond);
	pthread_mutex_unlock(((struct conn_info_wrapper*)parci)->mutex);
	request_header_parse(&ro);
	if (ro.method == METHOD_POST && ro.contentlength > 0) {
		request_body_parse(&ro);
	}
	
	if(si->request_callback) (si->request_callback)(&ro);
	
	close(ro.ci.conn);
	request_object_destroy(&ro);
	pthread_mutex_lock(&(si->currconn_mutex));
	if (si->currconn > 0) --(si->currconn);
	pthread_mutex_unlock(&(si->currconn_mutex));
	return NULL;
}

int server_accept_connection(struct server_info *si) {
	struct conn_info ci;
	ci.inaddr_size = sizeof(ci.inaddr);
	ci.conn = accept(si->sock,
			 (struct sockaddr*)&(ci.inaddr),&(ci.inaddr_size));
	if(ci.conn<0) {
		switch (errno) {
			case ENETDOWN:
			case EPROTO:
			case ENOPROTOOPT:
			case EHOSTDOWN:
			case ENONET:
			case EHOSTUNREACH:
			case EOPNOTSUPP:
			case ENETUNREACH:
				return 1;
			case EINTR:
				return 0;
			default:
				perror("server_accept_connection(): accept()");
				return 0;
		}
	}
	pthread_mutex_lock(&(si->currconn_mutex));
	if (si->currconn < si->maxconn) {
		++(si->currconn);
	} else {
		pthread_mutex_unlock(&(si->currconn_mutex));
		close(ci.conn);
		return 1;
	}
	pthread_mutex_unlock(&(si->currconn_mutex));
	pthread_attr_t attr;
	if (pthread_attr_init(&attr) != 0) {
		perror("server_accept_connection(): pthread_attr_init()");
		close(ci.conn);
		return 1;
	}
	if (pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED) != 0) {
		perror("server_accept_connection(): \
		pthread_attr_setdetachstate()");
		pthread_attr_destroy(&attr);
		close(ci.conn);
		return 1;
	}
	struct conn_info_wrapper ciw;
	ciw.si=si;
	ciw.ci=&ci;
	pthread_mutex_t mutex;
	pthread_cond_t cond;
	pthread_mutex_init(&mutex,NULL);
	ciw.mutex=&mutex;
	pthread_cond_init(&cond,NULL);
	ciw.cond=&cond;
	pthread_mutex_lock(ciw.mutex);
	if (pthread_create(&(ciw.thread),
		&attr,server_thread_process_request,(void*)&ciw) !=0 ) {
		perror("server_accept_connection(): pthread_create()");
	} else {
		pthread_cond_wait(ciw.cond, ciw.mutex);
	}
	pthread_mutex_unlock(ciw.mutex);
	pthread_cond_destroy(&cond);
	pthread_mutex_destroy(&mutex);
	pthread_attr_destroy(&attr);
	return 1;
}

void server_close_socket(int sock) {
	if(sock<0) return;
	shutdown(sock,SHUT_RDWR);
	close(sock);
}

int server_create_listener_socket(uint16_t port,int listenerqueue) {
	int sock;
	if ((sock = socket(AF_INET,SOCK_STREAM,0))<0) {
		perror("server_create_listener_socket(): socket()");
		return -1;
	}
	
	int yes = 1;
	if (setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(yes)) < 0) {
		perror("server_create_listener_socket(): setsockopt()");
		server_close_socket(sock);
		return -1;
	}
	
	struct sockaddr_in addr;
	memset(&addr,0,sizeof(addr));
	addr.sin_family=AF_INET;
	addr.sin_port=htons(port);
	addr.sin_addr.s_addr=INADDR_ANY;
	
	if (bind(sock,(struct sockaddr*)&addr,sizeof(addr)) < 0) {
		perror("server_create_listener_socket(): bind()");
		server_close_socket(sock);
		return -1;
	}
	
	if (listen(sock,listenerqueue) < 0) {
		perror("server_create_listener_socket(): listen()");
		server_close_socket(sock);
		return -1;
	}
	return sock;
}

void server_info_init(struct server_info *si,uint16_t port,int maxconn,
	void (*request_callback)(struct request_object*)) {
	si->sock = -1;
	si->port = port;
	si->maxconn = maxconn;
	si->currconn = 0;
	si->request_callback = request_callback;
	pthread_mutex_init(&(si->currconn_mutex),NULL);
}

void server_info_destroy(struct server_info *si) {
	server_close_socket(si->sock);
	pthread_mutex_destroy(&(si->currconn_mutex));
}

int server_weblisten(struct server_info* si) {
	if ((si->sock=server_create_listener_socket(si->port,si->maxconn))<0) {
		return 0;
	}
	
	printf("Server is starting on port %d.\n",si->port);
	while(server_accept_connection(si));
	printf("Server is going down.\n");
	return 1;
}
