#include "server.h"
#include "request.h"

#include <string.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>

struct server_info SI;

void die_handler(int signum) {
	(void)signum;
	server_info_destroy(&SI);
}

int signal_register() {
	struct sigaction dieaction;
	struct sigaction ignaction;
	memset(&dieaction,0,sizeof(dieaction));
	memset(&ignaction,0,sizeof(ignaction));
	dieaction.sa_handler = die_handler;
	ignaction.sa_handler = SIG_IGN;
	sigemptyset(&(dieaction.sa_mask));
	sigemptyset(&(ignaction.sa_mask));
	dieaction.sa_flags = 0;
	ignaction.sa_flags = 0;
	if (sigaction(SIGTERM,&dieaction,NULL) != 0
		|| sigaction(SIGINT,&dieaction,NULL) != 0
		|| sigaction(SIGHUP,&dieaction,NULL) != 0
		|| sigaction(SIGPIPE,&ignaction,NULL) != 0) {
		perror("sigaction()");
		return 0;
	}
	return 1;
}

void callback_func(struct request_object *ro) {
	char buffer[NI_MAXHOST];
	server_sprint_client_info(buffer,NI_MAXHOST,&(ro->ci.inaddr));
	printf("Connection from %s\n",buffer);
	printf("Method: %s\n",request_method_strings[ro->method]);
	printf("Path: %s\n",ro->path);
	request_parameter_nodes_print(ro->parameters);
}

int main() {
	server_info_init(&SI,8000,2,callback_func);
	if (!signal_register()) {
		return EXIT_FAILURE;
	}
	
	return !server_weblisten(&SI);
}
