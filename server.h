#ifndef _SERVER_H_
#define _SERVER_H_

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>

#include <stdint.h>
#include <pthread.h>

struct request_object;

struct server_info {
	int sock;
	uint16_t port;
	int maxconn;
	int currconn;
	pthread_mutex_t currconn_mutex;
	void (*request_callback)(struct request_object*);
};

struct conn_info {
	int conn;
	struct sockaddr_in inaddr;
	socklen_t inaddr_size;
};

void server_info_init(struct server_info *si,uint16_t port,int maxconn,
	void (*request_callback)(struct request_object*));
void server_info_destroy(struct server_info *si);
int server_weblisten(struct server_info* si);
int server_sprint_client_info(char *buffer,socklen_t size,
			       struct sockaddr_in *addr);

#endif
